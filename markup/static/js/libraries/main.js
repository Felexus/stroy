$(document).ready(function() {
    
    $(".collapsible-body").each(function() {
        var height = $(this).height();
        var body = $(this);
        $(this).css("height", 0);
        $(this).attr("data-height", height)
        // $(this).closest('.collapsible').find(".order__header").removeClass("active");
        var height = body.attr("data-height");
        // body.height(height);
    });
    
    $(document).on("click", ".collapsible-header", function() {
        var body = $(this).next(".collapsible-body");
        var header = $(this);
        if(header.hasClass("active")) {
            header.removeClass("active");
            var height = body.height();
            // body.attr("data-height", height);
            body.height(0);
            body.removeClass("active");
        } else {
            // $(".collapsible-header").removeClass("active");
            // var height = body.height();
            // $(".collapsible-body").height(0);
            // $(".collapsible-body").removeClass("active");
            header.addClass("active");
            var height = body.attr("data-height");
            body.height(height);
            console.log("21919921")
        }
    });

    $(".ctg__header").click(function() {
        $(this).toggleClass("active")
        $(".ctg__body").toggleClass("active")
    });


    if ($('#slider').length > 0) {
        var slider = document.getElementById('slider');
        var rangeMin = +$("#slider").attr("data-min")
        var rangeMax = +$("#slider").attr("data-max")

        var valMin = +$(".input-min").val();
        var valMax = +$(".input-max").val();
        
        var rangeStep = $("#slider").data("step")
        $(".output-left").text(parseFloat(valMin).toFixed(0));
        $(".output-right").text(parseFloat(valMax).toFixed(0));
        // $(".input-min").text(parseFloat(rangeMin).toFixed(0));
        noUiSlider.create(slider, {
            start: [valMin, valMax],
            connect: true,
            step: rangeStep,
            range: {
                'min': rangeMin,
                'max': rangeMax
            },
            format: wNumb({
                decimals: 0
            })
            
        });	

        slider.noUiSlider.on('slide', function () { 
            $(".noUi-handle-lower").each(function() {
                var val = +$(this).find("span").text();
                $(this).find("span").text(val.toFixed(0))
                console.log(val, 1);
                $(".filter-output-min").text(val.toFixed(0));
            });
        });
        slider.noUiSlider.on('slide', function () { 
            $(".noUi-handle-upper").each(function() {
                var val = +$(this).find("span").text();
                $(this).find("span").text(val.toFixed(0))
                console.log(val, 1);
                $(".filter-output-max").text(val.toFixed(0));
            });
        });

    }
    
})

/*
    This file can be used as entry point for webpack!
 */
